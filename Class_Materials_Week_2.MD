Внимание!
---------------

Поради технически проблеми няма да кача видео записи от този час. 
Ще опиша всичко достатъчно подробно с текст. 
Ако съм пропуснала нещо и не ви е ясно, пишете ми и ще го допълня. :)
Дори да сте присъствали в часа разгледайте практическата част, защото включва някои подобрения. :) 

План за седмица 2
----------------------------

1. Обсъждане на App Resources, Device Compatibility, User Interface, Layouts, Input Events

2. Практическа част
    - Създаване на layout
        * Елемент ястие
        * Списък с ястия
        * Детайлна страница на ястие
    - Java код за показване на едноколонен списък
    - Добавяне на Swipe-to-Refresh
    - Lint

----------------------------------------------

Теоретична част
----------------

[Link](https://docs.google.com/presentation/d/1j9o0MtVymJGLHhPCbVfkllE3PrUsIvHph4MWiLATTe8/edit?usp=sharing) към презентацията. 
В notes съм добавила бележки включващи това, за което съм говорила.

----------------------------------------------

Практическа част
================


Всяка седмица отделяме нов бранч за текущата седмица. Тази седмица работим на бранч week2.

Отваряме дизайна, за да сме наясно какво ще правим. Трябват ни следните картинки:
- За елемент ястие и за списък с ястия [link](https://trello-attachments.s3.amazonaws.com/5ad4902761dca5194cf92ca2/5ad4939202470d6c68fa7632/5fc8090c1862e67ad56e6c9541d66a0d/favourites.png)
- За детайлна страница на ястие [link](https://trello-attachments.s3.amazonaws.com/5ad4902761dca5194cf92ca2/5ad4939202470d6c68fa7632/d49fe352421800431a26dbb8bf05b200/meal-detailed_(1).png)


1.Създаване на layout файлове:
--------------------------------- 
Докато редактираме layout файловете следим в Preview дали всичко изглежда добре.

1.1. Елемент ястие 
------------------
- Десен бутон в/у папка layout  
- New > въвеждаме име *meal_item* > избираме за root елемент да се използва LinearLayout > OK > Yes
- Задаваме размер на ViewGroup-а, който в случая е LinearLayout. 
Той трябва да обгръща съдържащите се в него елементи по височина и да е широк колкото е широк неговия parent (колкото е широк екрана).

    ```
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    ```
    
- LinearLayout подрежда view-тата, които са в него последователно. 
Винаги задавайте ориентация - хоризонтално или вертикално.
Задаваме:
    ```
    android:orientation="vertical"
    ```

    ---------------------
- Добавяме ImageView като първи child елемент
- Задаваме му размери - височина 200dp, ширина колкото на екрана.
    ```
    android:layout_width="match_parent"
    android:layout_height="200dp"
    ```
- Добавяме му ID, за да можем после да го достъпим в Java кода
- Добавяме и атрибут с префикс tools за background, за да видим как изглежда ImageView-то. 
Повече по темата може да прочетете [тук](https://developer.android.com/studio/write/tool-attributes) в раздела Design-time view attributes. Всички атрибути зададени с префикс tools ни служат по време на дизайн и не се прилагат, когато се стартира приложението.

    ```
    tools:background="@color/colorPrimary" 
    ```
- Изписва ни, че не знае какво е tools. С Alt+Enter го добавяме.

    ---------------------
- Добавяме и TextView, в което ще изпишем заглавието
- Указваме размера му: Трябва да обгръща съдържанието си по височина и ширина.
    ```
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    ```
- Добавяме му ID, за да можем после да го достъпим в Java кода
- Центрираме го
    ```
    android:layout_gravity="center"
    ```
- Добавяме отстояние отгоре и отдолу
    ```
    android:layout_marginBottom="8dp"
    android:layout_marginTop="8dp"
    ```
- Задаваме размер на шрифта
    ```
    android:textSize="18sp"
    ```
- Задаваме примерен текст, за да видим как изглежда:
    ```
    tools:text="Some Title"
    ```
    ---------------------
- Готовият файл трябва да изглежда по следния начин [snippet](https://gitlab.com/snippets/1713322)
- Липсва сянката -> остава за домашно


1.2. Списък с ястия
-------------------
- Нека променим activity_main.xml, така че да показва само списък с ястия.
- Изтриваме всичко от текущия layout 
- За да покажем списък ни е необходимо само едно View - RecyclerView
- Добавяме го като root елемент
- Добавяме му ID, за да можем после да го достъпим в Java кода
- Размерите му са на цял екран:
    ```
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    ```
    
-  Добавяме специален атрибут с tools префикс, за да видим как ще изглежда списъка ако е пълен с елементи, чиито дизайн е дефиниран в item_meal.xml
    ```
    tools:listitem="@layout/item_meal"
    ```
    
- Готовият файл трябва да изглежда по следния начин [snippet](https://gitlab.com/snippets/1713335)


1.3. Детайли на ястие. 
---------------------
Пропускаме checkbox елементите от списъка Ingredients. Ще ги добавим на по-късен етап.

- Десен бутон в/у папка layout  
- New > въвеждаме име activity_detail.xml > избираме за root елемент да се използва LinearLayout > OK > Yes
- Задаваме размер на ViewGroup-а, който в случая е LinearLayout. 
Той трябва да обгръща съдържащите се в него елементи по височина и да е широк колкото е широк неговия parent (колкото е широк екрана).
    ```
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    ```
    
- Задаваме ориентация:
    ```
    android:orientation="vertical"
    ```
    
    ---------------------

- Добавяме ImageView като първи child елемент
- Задаваме му размери - височина 200dp, ширина колкото на екрана.
    ```
    android:layout_width="match_parent"
    android:layout_height="200dp"
    ```
- Добавяме му ID, за да можем после да го достъпим в Java кода
- Добавяме и атрибут с префикс tools за background, за да видим как изглежда ImageView-то. 

    ---------------------
- Добавяме и TextView, в което ще изпишем Cuisine
- Указваме размера му: Трябва да обгръща съдържанието си по височина и ширина.
    ```
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    ```
- Добавяме отстояние отгоре и в началото
    ```
    android:layout_marginStart="16dp"
    android:layout_marginTop="16dp"
    ```
- Задаваме текст. Тук използваме android: префикс, защото искаме този текст да се вижда от потребителите. Важно е да не е hardcode-нат, затова го extract-ваме в strings.xml и го реферираме. 
    
    ```
    android:text="@string/cuisine"
    ```
    
    Добавяме в strings.xml
    ```
    <string name="cuisine">Cuisine</string>
    ```
- Текстът трябва да е с главни букви, затова използваме атрибута
    ```
    android:textAllCaps="true"
    ```
- Задаваме цвят и стил bold
    ```
    android:textColor="@color/black"
    android:textStyle="bold"
    ```
    ---------------------

- Добавяме и TextView, в което ще изпишем съответната кухня
- Указваме размера му: Трябва да обгръща съдържанието си по височина и ширина.
- Добавяме отстояние отгоре и в началото
    ```
    android:layout_marginStart="16dp"
    android:layout_marginTop="8dp"
    ```
- Задаваме текст, като използваме tools: префикс.
    
    ```
    tools:text="American"
    ```
- Задаваме цвят
    ```
    android:textColor="@color/colorPrimaryDark"
    ```

- Готовият файл трябва да изглежда по следния начин [snippet](https://gitlab.com/snippets/1713324)


Java код за показване на едноколонен списък
----------------------------------------------------------------
Трябва да сте направили домашното си. По-конкретно урока [Creating Lists and Cards](https://developer.android.com/training/material/lists-cards.html). Ако не сте, внимателно прочетете всичко в урока и го направете. Това което следва надгражда урока и изисква неговото разбиране.

- Добавяме model клас за ястие. 
    - Десен бутон в/у пакета на проекта > New > Java class > въвеждаме Meal > OK. 
    - Класът има две полета - заглавие и картинка. 
    
        ```
        String title;
        String imageUrl;
        ```
        
    - Генерираме getters с Alt+Insert. (избираме опция Getter)
    - За сега неговите getter-и ще връщаш фиксирана стойност. 
    - Готовият файл трябва да изглежда по следния начин [snippet](https://gitlab.com/snippets/1713327)

-------------------------

- В MainActivity.java коментираме или премахваме onNavigationItemSelected логиката за сега. Целта е да покажем едноколонен списък. Правим го по аналогичен начин на урока. 

    - Променяме onCreate по следния начин: [snippet](https://gitlab.com/snippets/1713328).
    - Добавяме setupRecyclerView функцията [snippet](https://gitlab.com/snippets/1713326)
В нея създаваме списък от 5 елемента и ги подаваме към адаптера. Адаптера е от тип MealAdapter - нов клас, който следва да създадем.

------------------------

- Добавяме нов клас MealAdapter.java
- Десен бутон в/у пакета на проекта > New > Java class > въвеждаме MealAdapter > OK.
- Копираме кода от урока и го променяме, така че да се помазват не String елементи, а ястия. 
- Нашите данни не са от тип String както в примера, а от тип Meal.  Променяме String на Meal.
    ```
    private Meal[] mDataset;
    ```
    
- ViewHolder-а ни няма едно TextView, а включва TextView и ImageView. Добавяме го.

    ```
    public ImageView imageView;
    public TextView textView;
    ```
- Неговият конструктор не получава TextView, a View параметър. Променяме го. 
    ```
    public ViewHolder(View view) {...}
    ```
- Вътре в конструктора вземаме референции към TextView-то и ImageView-то чрез метода findViewById.
    ```
    imageView = view.findViewById(R.id.image_view);
    textView = view.findViewById(R.id.title_text_view);
    ```
- В onCreateViewHolder inflate-ваме R.layout.item_meal. Тук получаваме View, а не TextView като резултат от inflate. Променяме го.

    ```
    View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_meal, parent, false);
    ```
- В onBindViewHolder вземаме текущия елемент от списъка ни с Meals, който се намира на индекс position. 
    ```
    Meal meal = mDataset[position];
    ```
- Задаваме текст като достъпваме holder.textView.
    ```
    holder.textView.setText(meal.getTitle());
    ```
- За да зададем и картинка, първо добавяме библиотека за зареждане на картинки.  Добавяме Glide (или Picasso), следвайки указанията в сайта.
     - https://github.com/bumptech/glide
     - http://square.github.io/picasso/
- Зареждаме картинката. За Context използваме Context-a от ImageView-то.
    ```
    Glide.with(imageView.getContext())
                    .load(meal.getImageUrl())
                    .into(imageView);
    ```

- Готовият файл трябва да изглежда по следния начин [snippet](https://gitlab.com/snippets/1713330)
- Тестваме, но няма картинки Защо?


- [Добавяме Интернет разрешение в манифеста](https://developer.android.com/training/basics/network-ops/connecting.html)


Добавяне на Swipe-to-Refresh
----------------------------------------------------------------

- [Swipe-to-Refresh](https://developer.android.com/training/swipe/add-swipe-interface.html)
- Променяме activity_main.xml layout-a. Тъй като SwipeRefreshLayout е ViewGroup елемент, го дефинираме като обргаждащ елемент около добавения вече RecyclerView елемент. [snippet](https://gitlab.com/snippets/1713323)
- Тестваме и виждаме, че loader-а се показва и никога не се скрива.
- За да имитираме, че след определен период от време данните са обновени и да скрием loader-a променяме класа MainActivity по следния начин:
    - Добавяме функцията setupSwipeRefresh()
    - В нея вземаме референция към Layout-a

        ```
        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swiperefresh_layout);
        ```
    
    - Добавяме OnRefreshListener
    ```
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                
            }
        });
    ```
    - Добавяме Handler, който да спре loader-a след 500ms
    ```
    Handler mHandler = new Handler();//In UI Thread
    mHandler.postDelayed(() -> swipeRefreshLayout.setRefreshing(false), 500);
    ```
- Извикваме setupSwipeRefresh() в onCreate()
  
- Готовият файл трябва да изглежда по следния начин [snippet](https://gitlab.com/snippets/1713337)

Refactoring Shortcuts, които използвам:
-------------------------------------------

- [Method extraction shortcut](https://lh3.googleusercontent.com/-9QE0n8if48M/VEpNnAADJvI/AAAAAAAAOaA/hdn-oMyW-VA/w530-h416-n/53-extractmethod.gif) 

    Маркираме код, който искаме да се премести в нова функция и натискаме Ctrl+Alt+m.
    M идва от Method. Може и от менюто  Refactor > Extract > Method
 
- Аналогично могат да се extract-ват и променливи с Ctrl+Alt+v

Lint
------
Lint - е инструмент за анализиране на нашия код. [Тук](https://developer.android.com/studio/write/lint) е официалната документация

Как се използва? От менюто Analyze > Inspect Code

Можем да променим какво искаме да анализираме от трите точки до Inspection Profile и да си създадем наш профил.

Можем да сменим и scope-a на анализиране - само ресурси, само даден пакет и т.н. 

Натискаме OK и изчакваме. Излизат ни най-различни резултати. Когато маркираме дадена категория в дясно се появява пояснителен текст - защо това е проблем и обичайно има link с повече информация. 

Тук искам да отбележа, че не всички грешки трябва да бъдат поправени. Важно е обаче да ги разбираме. Това става с четене за всяка една и преценяване коя е важна от гледна точка на работоспособност, бързодействие, удобство за потребителите и т.н. 

Ето и чудесно видео, което покрива всичко описано по темата и още. :) 
[Using LINT for Performance Tips](https://www.youtube.com/watch?v=Z_huaXCsYyw)