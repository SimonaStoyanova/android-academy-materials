План за седмица 5
----------------------------

1. Обсъждане на:
    - Deep linking
    - HTTP request basics
    - JSON
    - Retrofit
    - Interceptors


2. Практическа част
    - JSON Parsing
    - GSON
    - Retrofit
    - Logging Interceptor
    - Empty screens

----------------------------------------------


Теоретична част
----------------

[Link](https://docs.google.com/presentation/d/1dkW9TvVzBvshgO3r29yFjWD973QFdwTWuYIWJZdJACg/edit?usp=sharing) към презентацията. В notes са включени бележки.


Практическа част
================

Всяка седмица отделяме нов branch за текущата седмица. Тази седмица работим на branch week5.


JSON Parsing
---------------
- API-я, с който ще работим е [този](https://gitlab.com/dev-labs-bg/mobile-academy-meals-api/blob/master/README.md)
- Извикваме метода [GET '/api/v1/meals](https://meals-api.devlabs.bg/api/v1/meals?id=52764)

- В build.gradle добавяме следната зависимост:

> testImplementation 'org.json:json:20140107'

за да работи json парсването в unit тестовете ни.

- Искаме да създадем нов unit test. Отваряме третата папка с името на пакета ви, до която пише (test). Тя съдържа клас ExampleUnitTest.java.

- Добавяме поле в класа `String json`, в което поставяме json-a, който се връща [тук](https://meals-api.devlabs.bg/api/v1/meals?id=52764)

```
String json = "{\n" +
            "  \"id\": \"52764\",\n" +
            "  \"title\": \"Garides Saganaki\",\n" +
            //...
            "  \"tags\": [\n" +
            "    \"Seafood\",\n" +
            "    \"Shellfish\"\n" +
            "  ]" +
            //...
            "}";
```

 Добавяме нова функция, в която прочитаме JSON обекта и проверяваме дали той има 2 тага.
 Опечатваме таговете му като масив и един по един.
```
@Test
public void parseMealTagsFromJson() throws JSONException {
    JSONObject rootJsonObject = new JSONObject(json);
    JSONArray tags = rootJsonObject.getJSONArray("tags");
    System.out.println("\nTags = " + tags);
    int size = tags.length();
    for (int i = 0; i < size; i++) {
        System.out.println("\nTag[" + i + "] = " + tags.get(i));
    }
    Assert.assertThat("How many are the tags? ", 2, is(tags.length()));
}
```
По този начин можем да парсваме обекти и да ги конвертираме в техния Java еквивалент. 

- Commit and Push

> Added parseMealTagsFromJson test

GSON
-----
Сега ще разгледаме една библиотека, която прави това за нас. 

Но преди да започнем, нека премахнем фалшивите данни от класа Meal.java
- Изтриваме всички getters
- Генерираме ги наново: Alt + Insert > Getter > Ctrl + A > OK
- Готово, вече имаме нормални getter-и

- Commit and Push

- Отваряме сайта на библиотеката - [GSON](https://github.com/google/gson)
В build.gradle добавяме:

> testImplementation 'com.google.code.gson:gson:2.8.4'

- Променили сме файла и отгоре ни излиза warning, че трябва да синхронизираме > Sync

- Добавяме нов тест:
```
@Test
public void parseMealGson() {
    Gson gson = new GsonBuilder().create();
    Meal meal = gson.fromJson(json, Meal.class);
    Assert.assertThat(meal.getTitle(), is(notNullValue()));
}
```
Стартираме го и виждаме следния exception:

```
com.google.gson.JsonSyntaxException: java.lang.IllegalStateException: Expected BEGIN_OBJECT but was STRING at line 15 column 6 path $.ingredients[0]
...
Caused by: java.lang.IllegalStateException: Expected BEGIN_OBJECT but was STRING at line 15 column 6 path $.ingredients[0]
```

Получаваме тази грешка, защото нашия модел Meal не отговаря на модела в JSON-а. Както виждате в JSON форматирания String имаме два масива - ingredients и measures, а в нашия модел има `List<Pair<String, String>>` ingredients. Нека за момент коментираме този списък от двойки и неговия getter, за да видим как работи библиотеката, а по-късно ще видим как да конвертираме данните, така че да ги попълним в списъка от Pairs.

Meal.java
```
//    List<Pair<String, String>> ingredients; //тук използваме класа android.support.v4.util.Pair


//    public List<Pair<String, String>> getIngredients() {
//        return ingredients;
//    }
```

- build-ваме проекта, но ни излиза build error, че вече нямаме  meal.getIngredients(), който се използва във функцията fillIngredients(). 

>error: cannot find symbol method getIngredients()	

- Double click-ваме върху error-a
- Коментираме тялото на функцията fillIngredients() и build-ваме наново.
- Готово, вече няма проблем. 
- По-късно ще добавим полето ingredients и ще върнем тялото на тази функция.

- Пускаме теста отново и виждаме, че той вече минава.
- Commit and Push

> Added parseMealTagsFromJson test

- Но както знаете класа има доста повече полета. Нека проверим дали всички те се парсват правилно.

```
@Test
public void parseMealGson() {
    Gson gson = new GsonBuilder().create();
    Meal meal = gson.fromJson(json, Meal.class);
    Assert.assertThat(meal.getTitle(), is(notNullValue()));
    System.out.println("\nTitle = " + meal.getTitle());
    Assert.assertThat(meal.getCuisine(), is(notNullValue()));
    System.out.println("\nCuisine = " + meal.getCuisine());
    Assert.assertThat(meal.getImageUrl(), is(notNullValue()));
    System.out.println("\nImageUrl = " + meal.getImageUrl());
    Assert.assertThat(meal.getInstructions(), is(notNullValue()));
    System.out.println("\nInstructions = " + meal.getInstructions());
    Assert.assertThat(meal.getOriginalRecipeUrl(), is(notNullValue()));
    System.out.println("\nOriginalRecipeUrl = " + meal.getOriginalRecipeUrl());
}
```

- Пускаме теста и ни излиза грешка
> java.lang.AssertionError: 
Expected: is not null
     but: was null
at bg.devlabs.androidacademy.ExampleUnitTest.parseMealGson(ExampleUnitTest.java:76)

- Натискаме на ExampleUnitTest.java:76 и ни отваря следния ред:

```
Assert.assertThat(meal.getImageUrl(), is(notNullValue()));
```

- Значи imageUrl не е било парснато. Нека погледнем дали го има в JSON string-a.
- Виждаме, че има такова поле, но името му е различно:

> "thumb": "https://www.themealdb.com/images/media/meals/wuvryu1468232995.jpg",

- Някак трябва да подскажем на библиотеката, че поле `thumb` в JSON-а отговаря на полето `imageUrl` в Meal.java
- В сайта на библиотеката има примери. Нека отворим [един клас модел](https://github.com/google/gson/blob/master/examples/android-proguard-example/src/com/google/gson/examples/android/model/Cart.java) и да видим как се указват ключовете, за да знае библиотеката кое на кое отговаря:

```
@SerializedName("buyer") 
private final String buyerName;
```
- Добавяме тази анотация и над нашите полета, които имат различни имена от тези, които API-я връща.
```
@SerializedName("thumb")
private String imageUrl;
```

- Излиза ни грешка:
> Cannot resolve symbol 'SerializedName'

- Това се случва, защото сме добавили зависимост към библиотеката само за тестовете
> testImplementation 'com.google.code.gson:gson:2.8.2'

- Отиваме в build.gradle и добавяме зависимост към библиотеката и за implementation:
>   implementation 'com.google.code.gson:gson:2.8.2'

- Променили сме файла и отгоре ни излиза warning, че трябва да синхронизираме > Sync
- Връщаме се в Meal.java и виждаме, че вече няма проблем
- Добавяме и още една анотация над originalRecipeUrl

```
    @SerializedName("source")
    private String originalRecipeUrl;
```

- Тъй като виждаме, че е null в отговора, премахваме условието да не е null в теста
>   Assert.assertThat(meal.getOriginalRecipeUrl(), is(notNullValue())); //изтриваме този ред
- Стартираме теста наново и вече виждаме всички стойности отпечатани в конзолата.

- Commit and Push

> Added parseMealGson test

Parse `List<Pair<String, String>>` ingredients 
------------------------
- В Meal.java разкоментираме полето и getter-a
- В MealActivity.java разкоментираме тялото на fillIngredients()
- Ако стартираме parseMealGson() теста сега, той ще fail-не, защото има поле със същото име, което е от друг тип

```
"ingredients": [
    "Raw king prawns",
    "Olive oil",
    "Chopped onion",
    "Freshly chopped parsley",
    "White wine",
    "Chopped tomatoes",
    "Minced garlic",
    "Cubed Feta cheese"
  ],
```

- За да спрем парсването на полето трябва да добавим ключовата дума transient пред него:

> transient `List<Pair<String, String>>` ingredients; //тук използваме класа android.support.v4.util.Pair

- Стартираме теста parseMealGson() и виждаме, че вече няма проблем.

- Commit and Push

> Returned the ingredients field to Meal.java and added transient keyword to stop the serialization and deserialization of the field

- А сега да парснем полето сами.
- Добавяме setter на ingredients в Meal.java с Alt+Insert > Setter > ingredients > OK

- Добавяме throws JSONException към дефиницията на метода parseMealGson()
- Парсваме двата списъка:

```
JsonObject rootJsonObject = new JsonParser().parse(json).getAsJsonObject();
JsonArray ingredientsApi = rootJsonObject.getAsJsonArray("ingredients");
JsonArray measures = rootJsonObject.getAsJsonArray("measures");
int size = ingredientsApi.size();
List<Pair<String, String>> ingredients = new ArrayList<>(size);
for (int i = 0; i < size; i++) {
    //тук използваме класа android.support.v4.util.Pair
    ingredients.add(new Pair<>(measures.get(i).getAsString(), ingredientsApi.get(i).getAsString())); 
}
```


- Парсваме Meal с GSON и след това му задаваме списъка, който създадохме в предишната стъпка.
```
Meal meal = gson.fromJson(json, Meal.class);
meal.setIngredients(ingredients);
```

- Добавяме проверка дали не е null и го отпечатваме в края на функцията
```
Assert.assertThat(meal.getIngredients(), is(notNullValue()));
System.out.println("\nIngredients = " + meal.getIngredients());
```
- Build-ваме и виждаме, че са се добавили и се оптечатват в конзолата

- Commit and Push
> Added ingredients parsing logic


Да подредим проекта: 
---------------------
- С десен бутон в/у името на проекта > New > Package създаваме следните пакети:
  - data
  - ui

- С десен бутон в/у името на пакета data и ui > New > Package създаваме следните пакети:
    - data
        - model
    - ui
        - base
        - home

- Преместваме класовете с провлачване и натискаме Refactor в диалога, който иска потвърждение.
Ако ни излезе диалог с title Problems Detected натискаме Continue, защото изписаните грешки са в генерирани класове, които ще се генерират наново с новата структура и няма да бъдат проблем.
След Continue ни излиза прозорец долу на който натискаме `Do Refactor`.
    - data
        - model (Meal)
    - ui
        - base (MealsFragment)
        - home (MealAdapter, MainActivity, LatestFragment, RandomFragment, FavoritesFragment)

- След преместване в MealsFragment ни излиза грешка, че не може да достъпи адаптера MealAdapter. С Alt+Insert го правим public

- След преместване на всички файлове, те трябва да са оцветени в син цвят. Ако напишете git status да ви излиза списък от файлове, пред които пише modified. 

```
$ git status

On branch week5_prep
Your branch is up-to-date with 'origin/week5_prep'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

renamed:    app/src/main/java/bg/devlabs/androidacademy/Meal.java -> app/src/main/java/bg/devlabs/androidacademy/data/Meal.java
renamed:    app/src/main/java/bg/devlabs/androidacademy/MealsFragment.java -> app/src/main/java/bg/devlabs/androidacademy/ui/base/MealsFragment.java
renamed:    app/src/main/java/bg/devlabs/androidacademy/FavoritesFragment.java -> app/src/main/java/bg/devlabs/androidacademy/ui/home/FavoritesFragment.java
...

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

modified:   app/src/main/AndroidManifest.xml
modified:   app/src/main/java/bg/devlabs/androidacademy/MealActivity.java
...

```

Ако някой от файловете ви е оцветен в зелено. Когато напишете git status ще го видите в първия списък да излиза като нов файл (new file), а във втория списък като изтрит (deleted). 

```
new file:   app/src/main/java/bg/devlabs/androidacademy/data/Meal.java
...
deleted:   app/src/main/java/bg/devlabs/androidacademy/Meal.java
```
 
Android Studio не е разбрало, че е преместен и мисли че има нов файл, а стария е изтрит. За да оправите проблема преди да commit-нете, изпълнете  `git add <стария път до файла>` през конзолата:

```
git add app/src/main/java/bg/devlabs/androidacademy/data/Meal.java
```
- Проверете отново с git status, за да видите дали се е променило, че файла е преместен, а не е нов файл. Може да се наложи да добавите и новия файл с git add.

Ако всички файлове са ви оцветени в синьо и излизат като modified и renamed. Направете 

- Commit и Push

> Changed packages structure

Retrofit 
---------

Да направим първата си заявка (get latest)

Както знаете всичкия код, който пишем се изпълнява на основната нишка main thread. Когато правим заявка към интенет обаче, не искаме да блокираме потребителския интерфейс, защото потребителя ще помисли, че приложението е замръзнало, т.е. не отговаря и най-вероятно ще го убие и деинсталира.

Затова заявките към Интернет и другите времеемки задачи се изпълняват на други background нишки.
  - Отваряме сайта на библиотеката, за да видим как да я добавим и как да я използваме [Retrofit](http://square.github.io/retrofit/)
  - Добавяме зависимост в build.gradle

  > compile 'com.squareup.retrofit2:retrofit:2.4.0'

  - Sync
  - Излиза ни warning, че вече не се използва ключовата дума compile 

  > Configuration 'compile' is obsolete and has been replaced with 'implementation' and 'api'.
It will be removed at the end of 2018. For more information see: http://d.android.com/r/tools/update-dependency-configurations.html

  - заменяме compile с implementation

  > implementation 'com.squareup.retrofit2:retrofit:2.4.0'

  - Sync

  - Commit and Push

> Added Retrofit.

  - Отново отваряме сайта и виждаме, че е необходимо да добавим Java interface за дефиниране на заявките.
  - Десен бутон на пакета data > New > Package > remote > OK
  - Десен бутон на пакета remote > New > Java class > MealsService > Kind: Interface > OK
  - Отваряме документацията на API-я, който ще използваме - [mobile-academy-meals-api](https://gitlab.com/dev-labs-bg/mobile-academy-meals-api/blob/master/README.md)
  - копираме дефиницията на request-а от примера и го променяме

  ```
  @GET("users/{user}/repos")
  Call<List<Repo>> listRepos(@Path("user") String user);
  ```

  - Методът е същия - GET
  - Променяме пътя, като внимаваме къде се слагат наклонени черти в началото и края и къде не
  api/v1/latest
  - Върнатия резултат ще е списък от ястия `Call<List<Meal>>`
  - Но тези ястия не съдържат всички полета, които имаме дефинирани
  - Добавяме нов модел. Може да използваме jsonschema2pojo или сами да напишем модела.
  - Десен бутон на пакета data > New > Package > model > OK
  - Преместваме Meal.java в новия пакет model
  - Десен бутон на пакета model > New > Java class > MealSimple > OK
  - Копираме един обект от JSON-a и поставяме в jsonschema2pojo
  - Class name: MealSimple
  - Source type: JSON
  - Annotation style: Gson
  - Маркираме Use primitive types
  - Preview

```
public class MealSimple {
  @SerializedName("id")
  @Expose
  public String id;
  @SerializedName("title")
  @Expose
  public String title;
  @SerializedName("thumb")
  @Expose
  public String thumb;
  @SerializedName("youtube")
  @Expose
  public Object youtube;
  @SerializedName("tags")
  @Expose
  public List<String> tags = null;
}
```
- Полето youtube обаче е от тип String, променяме го.
- Преименуваме полетата (thumb -> imageUrl, youtube -> youtubeUrl)
- Генерираме getters: Alt + Insert > Getter > Ctrl + A > OK
- Commit and Push като маркираме само класа MealSimple.java

> Added MealSimple.java.

- Връщаме се в MealsService интерфейса
- Върнатия резултат ще е списък от ястия от новия клас MealSimple
`Call<List<MealSimple>>`
- Името на функцията ще е getLatest
- Не се подават никакви параметри, затова махаме параметрите от тялото на функцията

```
public interface MealsService {
  @GET("/api/v1/latest")
  Call<List<MealSimple>> getLatest();
}
```

- Commit and Push

> Added MealsService interface with the first call - getLatest. Package reformatting.

Make the first call
---------------------
Да направим заявката getLatest и да покажем резултата на екрана.
- Отново отваряме сайта и виждаме, как се прави заявка:

```
Retrofit retrofit = new Retrofit.Builder()
    .baseUrl("https://api.github.com/")
    .build();

GitHubService service = retrofit.create(GitHubService.class);
Call<List<Repo>> repos = service.listRepos("octocat");
```

- Отиваме в Latest Fragment или в BaseFragment, в зависимост дали сме изнесли общата логика в базов клас 

Премахваме hardcoded списъка от  setupRecyclerView()

```
private void setupRecyclerView() {
    // ...
    recyclerView.setHasFixedSize(true);
    RecyclerView.Adapter mAdapter = new MealAdapter(new ArrayList<>());
    recyclerView.setAdapter(mAdapter);
}
```

- Изнасяме полето mAdapter да е поле на класа и го преименуваме на аdapter. 
- Променяме типа му на MealAdapter.

```
MealAdapter adapter;

...
private void setupRecyclerView() {
    // Use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    adapter = new MealAdapter(new ArrayList<>());
    recyclerView.setAdapter(adapter);
}
```

- Ако адаптера ни е написан с масив, а не с List, го променяме, да приема List в конструктора си. Полето му с данни също става List. Вземането на размер става с метода List.size(), а на елемент на дадена позиция става с метода List.get(position). Също така променяме типа на елементите да е MealSimple. 

- Build-ваме
- Виждаме, че има проблеми с методите за стартиране на MealActivity.
- За сега променяме типа на MealSimple и коментираме проблемните редове.

-------------------------

- Добавяме метод fetchMeals(). В него ще пишем заявката. Извикваме го в onCreateView().
- Build-ваме Retrofit, като внимаваме за това къде има и къде няма наклонени черти в base URL-a

```
Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("https://meals-api.devlabs.bg/")
        .build();
```

- Създаваме MealsService

```
MealsService service = retrofit.create(MealsService.class);
```

- Дефинираме заявката

```
Call<List<MealSimple>> latestMeals = service.getLatest();
```

- Извикваме я с метода enqueue който асинхронно изпраща заявката и извиква callback-а, когато получи отговор или ако настъпи грешка

```
latestMealsCall.enqueue(new Callback<List<MealSimple>>() {
            @Override
            public void onResponse(Call<List<MealSimple>> call, Response<List<MealSimple>> response) {
                
            }

            @Override
            public void onFailure(Call<List<MealSimple>> call, Throwable t) {

            }
        });
```

- Ако получим отговор, че е настъпила грешка показваме Toast

```
@Override
public void onFailure(Call<List<MealSimple>> call, Throwable t) {
    Toast.makeText(getContext(), R.string.network_error, Toast.LENGTH_SHORT).show(); //Please try again later
}
```

- Когато получим отговор с ястия ги показваме. Задаваме елементите на адаптера и му казваме да се обнови.

```
@Override
public void onResponse(Call<List<MealSimple>> call, Response<List<MealSimple>> response) {
    // Set items and refresh the adapter
    adapter.setItems(response.body());
    adapter.notifyDataSetChanged();
}
```

- Да, но не сме добавили метод setItems() в адаптера.
- Добавяме го с ALt+Enter > Create Method

```
public void setItems(List<MealSimple> meals) {
    mDataset = meals;
}
```

- Run
- Получаваме следния Crash

>  Caused by: java.lang.IllegalArgumentException: Unable to create converter for java.util.List<bg.devlabs.androidacademy.data.model.MealSimple>

- Забравили сме да добавим GSON конвертор-а
- В build.gradle добавяме

```
// JSON Converter
compile 'com.squareup.retrofit2:converter-gson:2.4.0'
```

- Sync Now
- Добавяме GsonConverterFactory при build-ването на Retrofit

```
Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("https://meals-api.devlabs.bg/")
        .addConverterFactory(GsonConverterFactory.create())
        .build();
```

- Run
- Готово! Виждаме списък с ястия получени от Интернет.
- Commit and Push

>Added network call and showing the result in the meals list
Changed the MealAdapter to use the new model
Added JSON Converter for Retrofit

Logging Interceptor
-------------------
Както вече ви споменах Retrofit изпозлва OkHttp, за да прави заявките към web сървъра.
По-конкретно използва класа OkHttpClient. Всеки OkHttpClient позволява добавяне на Interceptor-и. Ние ще добавим един, който ще отпечатва заявките към и отговорите от сървъра в logcat-a, за да можем да следим какво се случва.

- В build.gradle добавяме следното dependency

```
implementation 'com.squareup.okhttp3:logging-interceptor:3.10.0'
```

- Sync Now
- Сега отиваме там, където се build-ва Retrofit и му задаваме нов OkHttpClient, на който предварително сме задали да има HttpLoggingInterceptor. Това става по следния начин:

```
OkHttpClient.Builder client = new OkHttpClient.Builder();
HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
client.addInterceptor(loggingInterceptor);

Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("https://meals-api.devlabs.bg/")
        .client(client.build())
        .addConverterFactory(GsonConverterFactory.create())
        .build();
 
```

- Пускаме приложението и следим в logcat-a какви заявки се изпращат и какви отговори се връщат.

- Commit and Push

> Added Logging Interceptor

Swipe to Refresh
-----------------
Нека да имплементираме swipe to refresh логиката - да правим заявка и да обновяваме екрана

- Първо премахвам getMealCount() логиката, защото тя ни трябваше докато работихме с фалшиви данни.
- Commit and Push

> code cleaning

--------------------

- Променяме функцията setupSwipeRefresh()

```
/**
  * Refreshes content by making a new API call and shows the result in the list
  */
private void setupSwipeRefresh() {
    swipeRefreshLayout.setOnRefreshListener(() -> {
        fetchMeals();
    });
}
```

- Но сега loader-а няма да се скрие, в края на заявката
- За да решим проблема ще добавим android.os.handler.Callback параметър към fetchMeals() и изпращаме съобщение при отговор от сървъра (в onResponse) или при грешка (в onFailure).

```
Call<List<MealSimple>> call = service.getLatest();
call.enqueue(new Callback<List<MealSimple>>() {
    @Override
    public void onResponse(Call<List<MealSimple>> call, Response<List<MealSimple>> response) {
        callback.handleMessage(new Message());
        adapter.setItems(response.body());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFailure(Call<List<MealSimple>> call, Throwable t) {
        callback.handleMessage(new Message());
        Toast.makeText(getContext(), R.string.network_error, Toast.LENGTH_SHORT).show();
    }
});
```

- Добавяме Callback параметъра и имплементираме скриване, когато се извика

```
fetchMeals(new Handler.Callback() {
    @Override
    public boolean handleMessage(Message msg) {
        swipeRefreshLayout.setRefreshing(false);
        return false;
    }
});
```

- Заменяме с lambda

```
/**
  * Refreshes content by making a new API call and shows the result in the list
  */
private void setupSwipeRefresh() {
    swipeRefreshLayout.setOnRefreshListener(() -> fetchMeals(msg -> {
        swipeRefreshLayout.setRefreshing(false);
        return false;
    }));
}
```

- Добавяме callback-a и при извикването на fetchMeals в onCreateView. Тук няма някаква специална логика и на практика не използваме callback-a. Заменяме с lambda.

```
fetchMeals(msg -> false);
```

- Тестваме като влизаме без Интернет, пускаме Интернетa и swipe-ваме

- Commit and Push
>Added swipe to refresh logic
Added setItems method to MealAdapter.java

Fix MealActivity start()
-------------------------

Помните ли, че преди малко закоментирахме част от екстрите, които се подаваха?
Нека поправим кода. Този екран ще се стартира с подаването единствено на id на ястието. Останалата информация ще получаваме със заявка към сървъра.

- Премахваме дефинираните екстри и добавяме нова:

```
private static final String EXTRA_MEAL_ID = "bg.devlabs.androidacademy.meal.id";
```

- Обновяваме start метода

```
public static void start(Context context, MealSimple meal) {
    Intent starter = new Intent(context, MealActivity.class);
    starter.putExtra(EXTRA_MEAL_ID, meal.getId());
    context.startActivity(starter);
}
```

- Вместо fillUiFromIntent(); извикваме нова функция fetchMeal() в onCreate:

```
@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);
    ButterKnife.bind(this);
    fetchMeal();
    setupViews();
}
```

- В нея за домашно ще добавите заявката и ще попълните екрана с резултата, използвайки променена версия на fillUiFromIntent, с ново име fillUi(), която приема Meal и използвайки полетата му показва данните в потребителския интерфейс.

```
 private void fillUi( Meal meal) {
    String title = meal.getTitle();
    String imageUrl = meal.getImageUrl();
    String cuisine = meal.getCuisine();
    String instructions = meal.getInstructions();
    String originalRecipeUrl = meal.getOriginalRecipeUrl();
    ...
 }


private void fetchMeal() {
    //todo use fillUi when you get a Meal result
}
```

- Налага се да променим и shareMeal()
- Добавяме две полета в класа

```
String title = "", originalRecipeUrl = "";
```

- използваме ги в shareMeal()

```
private void shareMeal() {
    Intent sendIntent = new Intent();
    sendIntent.setAction(Intent.ACTION_SEND);
    String shareText = String.format(getString(R.string.share_meal_text),
            title, originalRecipeUrl);
    sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.cool_recipe));
    sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
    sendIntent.setType("text/plain");
    startActivity(sendIntent);
}
```

- Когато получим данни за ястието трябва да им дадем стойност

MealActivity empty screen
----------------------------------
- И последно да добавим empty screen към екрана MealActivity. 
- Отваряме activity_detail.xml
- Добавяме FrameLayout около целия layout и include на празния екран:

```
 <FrameLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <include layout="@layout/empty_no_connection"/>
    <android.support.design.widget.CoordinatorLayout
    ...
</FrameLayout>
```


 - Проверяваме за налична връзка към Интернет. Ако има правим заявка, ако няма показваме празни екран.

```
if (isNetworkAvailable()) {
    fetchMeal();
} else {
    showEmptyScreen();
}
```

- Добавяме функцията за проверка за налична връзка към Интернет.

```
private boolean isNetworkAvailable() {
    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(
            Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = null;
    if (connectivityManager != null) {
        activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    }
    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
}
```

- Добавяме Butterknife Injection на empty state екрана. 
Alt + Enter > Generate Butterknife injections > Маркираме id-то на най-външното View в екрана ни за empty state. При мен се казва empty_state_container и е LinearLayout

```
@BindView(R.id.empty_state_container)
LinearLayout emptyStateContainer;
```

- Дефинираме функцията за показване на празния екран.

```
private void showEmptyScreen() {
    coordinator.setVisibility(View.GONE);
    emptyStateContainer.setVisibility(View.VISIBLE);
}
```

- Оставям на вас да напишете логиката за работа на двата бутона Refresh и Settings.
- Commit and Push

> Fixed Meal Activity start logic
Added empty screen showing