План за седмица 4
----------------------------

1. Обсъждане на:
    - Processes and Threads
    - User Interface
      - Menus
      - Dialogs
      - Toasts
      - Snackbar
    - Sharing Simple Data


2. Практическа част
    - Caching Fragments
    - Add Views at Runtime
    - Build problems
    - App Crashes


----------------------------------------------

Теоретична част
----------------

[Link](https://docs.google.com/presentation/d/19k20XH3ifBsJnd7XgsotLZdkVjhMpMx7mltTsuek9xI/edit?usp=sharing) към презентацията. В notes са включени бележки.

----------------------------------------------


Практическа част
================

Всяка седмица отделяме нов бранч за текущата седмица. Тази седмица работим на бранч week4.

Отваряме дизайна на детайлна страница на ястие: [link](https://trello-attachments.s3.amazonaws.com/5ad4902761dca5194cf92ca2/5ad4939202470d6c68fa7632/d49fe352421800431a26dbb8bf05b200/meal-detailed_(1).png)


1.Домашно
--------------

Първо трябва да сме сигурни че всички са направили всичко, което беше за домашно.

 2.Кеширане на фрагменти
--------------
 Как да използваме кеширани фрагментите при смяна от bottom navigation в MainActivity.
  
- Променяме showFragment() да използва transaction.add() и подаваме tag - името на фрагмента
 ```
private void showFragment(Fragment fragment) {
    FragmentManager manager = getSupportFragmentManager();
    FragmentTransaction transaction = manager.beginTransaction();
    String tag = fragment.getClass().getSimpleName();
    transaction.add(R.id.frame_layout, fragment, tag);
    transaction.commit();
}
  ```
  
- Commit 
> Changed showFragment() to use transaction.add() and passed a tag
- Push 

- Добавяме нова фунция, която проверява дали даден фрагмент вече е добавен.
Ако е добавен, го показва, ако не е - го добавя. 
  Тази функция използва подадените ѝ параметри по следния начин:
  - FragmentManager - определя дали фрагмента вече е добавен според неговия tag:
  - FragmentTransaction - към който прилага промените
  - Fragment - за да определи tag и за да го добави, ако не е бил добавян до сега

```
 private static void checkCacheAdd(FragmentManager fragmentManager,
                                    FragmentTransaction transaction, Fragment fragment) {
    String tag = fragment.getClass().getSimpleName();
    Fragment cachedFragment = fragmentManager.findFragmentByTag(tag);
    List<Fragment> addedFragments = fragmentManager.getFragments();
    for (Fragment tempFragment : addedFragments) {
        if (tempFragment.isVisible())
            transaction.hide(tempFragment);
    }
    if (cachedFragment != null) {
        transaction.show(cachedFragment);
    } else {
        transaction.add(R.id.frame_layout, fragment, tag);
    }
}
```
- Променяме логиката за показване на фрагменти да изпозлва новия метод

```
@Override
public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction transaction = fragmentManager.beginTransaction();
    switch (item.getItemId()) {
        case R.id.navigation_latest:
            checkCacheAdd(fragmentManager, transaction, LatestFragment.newInstance());
            break;
        case R.id.navigation_random:
            checkCacheAdd(fragmentManager, transaction, RandomFragment.newInstance());
            break;
        case R.id.navigation_favorites:
            checkCacheAdd(fragmentManager, transaction, FavoritesFragment.newInstance());
            break;
    }
    transaction.commit();
    return true;
}
```

- Commit
> Added fragments caching and using the cached fragments if possible instead of replacing with new instances all the time

- Push 

 3.Add Views at Runtime
--------------
- Да покажем ingredients.
- Добавяме ново поле в Meal.java. Използваме класа android.support.v4.util.Pair

```
  List<Pair<String, String>> ingredients;
```
- Добавяме default ingredients в Meal.java. Използваме класа android.support.v4.util.Pair

```
public List<Pair<String, String>> getIngredients() {
    List<Pair<String, String>> ingredients = new ArrayList<>(5);
    ingredients.add(new Pair<>("100g", "Flour")); 
    ingredients.add(new Pair<>("2", "Large Eggs"));
    ingredients.add(new Pair<>("300ml", "Milk"));
    ingredients.add(new Pair<>("1tbsp", "Sunflower Oil"));
    ingredients.add(new Pair<>("to serve", "Sugar"));
    return ingredients;
}
```

- Commit
> Added ingredients field and default ingredients to Meal

- Push 

- В activity_detail.xml добавяме id на LinearLayout контейнера, тъй като ще добавяме view-та в него

```
<android.support.v4.widget.NestedScrollView
...
    >

    <LinearLayout
        android:id="@+id/container"
        ...>
        ...
```
- Добавяме и BindView дефиниция в MealActivity.java с Alt + Insert > Generate Butterknife Injections

```
@BindView(R.id.container)
LinearLayout container;
```

- Обхождаме списъка от двойки и за всеки елемент създаваме нов check box и го добавяме в контейнера под индекс началния + текущия индекс от итерацията. Използваме класа android.support.v4.util.Pair

```
private void fillIngredients() {
    Meal meal = new Meal();
    List<Pair<String, String>> ingredients = meal.getIngredients();
    int size = ingredients.size();
    for (int i = 0; i < size; i++) {
        Pair<String, String> pair = ingredients.get(i);
        CheckBox checkBox = new CheckBox(this);
        checkBox.setText(String.format("%s %s", pair.first, pair.second));
        container.addView(checkBox, 3 + i);
    }
}
```

- Извикваме функцията във fillUiFromIntent()

- Commit
> Added ingredients showing logic to MealActivity.java
- Push 

 4.Build problems
--------------

Пример - липсващи дефиниции в layout

- Отваряме файла activity_main.xml
- Махаме id атрибута на FrameLayout-a 

```
android:id="@+id/frame_layout"
```

- Затваряме всички отворени файлове
- Натискаме на build иконката и ни изписва 
    > Compilation failed
- Отваря се прозореца build и отиваме до JavaCompiler (1 error)
- Разтваряме дървото с пътя към грешката и достигаме до
    > error: cannot find symbol variable frame_layout	
- Натискаме два пъти и сме отведени до мястото където това id се ползва, а не съществува
- Изпълнявам Git > Revert, но проблема си стои
- Чак след като build-на наново ще се установи, че вече има такова id

 5.App Crashes
--------------

5.1. Пример 1

- Отваряме файла MainActivity.java
- Променяме типа на BottomNavigationView-то на LinearLayout

```
@BindView(R.id.navigation)
BottomNavigationView navigation;
```

```
@BindView(R.id.navigation)
LinearLayout navigation;
```
- Коментираме setupBottomNavigation() функцията и реда, в който се извиква в onCreate()

```
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ...
//        setupBottomNavigation();
    }

//    private void setupBottomNavigation() {
//        navigation.setOnNavigationItemSelectedListener(this);
//    }
```

- Затваряме всички отворени файлове
- Натискаме на build иконката и build-a минава
- Сега е време да стартираме
> Run 'app' > OK
- На емулатора излиза съобщение в диалог
> Tasty App has stopped
- Отваряме Logcat
- Избираме Error като тип на съобщенията, които да се показват
- Скролваме нагоре докато не намерим лог за грешка хвърлен от нашето приложение
> bg.devlabs.androidacademy E/AndroidRuntime: FATAL EXCEPTION: main
Process: bg.devlabs.androidacademy, PID: 32405
    java.lang.RuntimeException: Unable to start activity ComponentInfo{bg.devlabs.androidacademy/bg.devlabs.androidacademy.MainActivity}: java.lang.IllegalStateException: View 'navigation' with ID 2131230828 for field 'navigation' was of the wrong type. See cause for more info.
- Прочитаме съобщението и разбираме, че View 'navigation' was of the wrong type. 
- Сега виждаме, че има и два линка сочещи към файлове, които са оцветени в синьо
- Когато видим такива линкове е добре да ги отворим. Те сочат към реда от кода ни, който е създал грешката.
- Натискаме на първия
> (MainActivity_ViewBinding.java:25)

Отваря се генерирания метод за Bind-ване на View и можем да видим, че типът не е Bottom Navigation

> target.navigation = Utils.findRequiredViewAsType(source, R.id.navigation, "field 'navigation'", LinearLayout.class); 

- Натискаме и на втория линк
> (MainActivity.java:25)

Отваря ни класа и измества курсора до ред 25
> ButterKnife.bind(this);

Значи имаме проблем с Butterknife bind-ването, знаем че View 'navigation' е било от грешен тип. Видяли сме и че в генерирания клас, класа е LinearLayout. Това можем да проверим и в дефиницията в MainActivity. И е ясно какъв е проблема.
- Изпълнявам Git > Revert

-------------------------------------------

5.2. Пример 2

- Отваряме файла MealAdapter.java
- В метода getItemCount() добавяме деление на 0

```
@Override
public int getItemCount() {
    return mDataset.length/0;
}
```
- Затваряме всички отворени файлове
- Натискаме на build иконката и build-a минава
- Сега е време да стартираме
> Run 'app' > OK
- На емулатора излиза съобщение в диалог
> Tasty App has stopped
- Отваряме Logcat
- Избираме Error като тип на съобщенията, които да се показват
- Скролваме нагоре докато не намерим лог за грешка хвърлен от нашето приложение
> bg.devlabs.androidacademy E/AndroidRuntime: FATAL EXCEPTION: main
    Process: bg.devlabs.androidacademy, PID: 1846
    java.lang.ArithmeticException: divide by zero
- Прочитаме съобщението и разбираме, че има ArithmeticException: деление на нула. 
- Сега виждаме, че има и линк сочещ към (MealAdapter.java:65)
- Натискаме го и ни отваря точния ред, в който сме добавили деление на 0.
- Изпълнявам Git > Revert

6. Commit and push
-------------------------
